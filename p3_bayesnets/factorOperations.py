# factorOperations.py
# -------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from bayesNet import Factor
import operator as op
import util
import functools

def joinFactorsByVariableWithCallTracking(callTrackingList=None):


    def joinFactorsByVariable(factors, joinVariable):
        """
        Input factors is a list of factors.
        Input joinVariable is the variable to join on.

        This function performs a check that the variable that is being joined on 
        appears as an unconditioned variable in only one of the input factors.

        Then, it calls your joinFactors on all of the factors in factors that 
        contain that variable.

        Returns a tuple of 
        (factors not joined, resulting factor from joinFactors)
        """

        if not (callTrackingList is None):
            callTrackingList.append(('join', joinVariable))

        currentFactorsToJoin =    [factor for factor in factors if joinVariable in factor.variablesSet()]
        currentFactorsNotToJoin = [factor for factor in factors if joinVariable not in factor.variablesSet()]

        # typecheck portion
        numVariableOnLeft = len([factor for factor in currentFactorsToJoin if joinVariable in factor.unconditionedVariables()])
        if numVariableOnLeft > 1:
            print("Factor failed joinFactorsByVariable typecheck: ", factor)
            raise ValueError("The joinBy variable can only appear in one factor as an \nunconditioned variable. \n" +  
                               "joinVariable: " + str(joinVariable) + "\n" +
                               ", ".join(map(str, [factor.unconditionedVariables() for factor in currentFactorsToJoin])))
        
        joinedFactor = joinFactors(currentFactorsToJoin)
        return currentFactorsNotToJoin, joinedFactor

    return joinFactorsByVariable

joinFactorsByVariable = joinFactorsByVariableWithCallTracking()


def joinFactors(factors):
    """
    Question 3: Your join implementation 

    Input factors is a list of factors.  
    
    You should calculate the set of unconditioned variables and conditioned 
    variables for the join of those factors.

    Return a new factor that has those variables and whose probability entries 
    are product of the corresponding rows of the input factors.

    You may assume that the variableDomainsDict for all the input 
    factors are the same, since they come from the same BayesNet.

    joinFactors will only allow unconditionedVariables to appear in 
    one input factor (so their join is well defined).

    Hint: Factor methods that take an assignmentDict as input 
    (such as getProbability and setProbability) can handle 
    assignmentDicts that assign more variables than are in that factor.

    Useful functions:
    Factor.getAllPossibleAssignmentDicts
    Factor.getProbability
    Factor.setProbability
    Factor.unconditionedVariables
    Factor.conditionedVariables
    Factor.variableDomainsDict
    """

    # typecheck portion
    setsOfUnconditioned = [set(factor.unconditionedVariables()) for factor in factors]
    if len(factors) > 1:
        intersect = functools.reduce(lambda x, y: x & y, setsOfUnconditioned)
        if len(intersect) > 0:
            print("Factor failed joinFactors typecheck: ", factor)
            raise ValueError("unconditionedVariables can only appear in one factor. \n"
                    + "unconditionedVariables: " + str(intersect) + 
                    "\nappear in more than one input factor.\n" + 
                    "Input factors: \n" +
                    "\n".join(map(str, factors)))


    "*** YOUR CODE HERE ***"
    
    # Inicializacion
    uncondicionedSet = set()
    condicionedSet = set()

    # Convierte la entrada factors en una lista
    factors = list(factors)

    # Iteran a través de cada factor y actualizan los conjuntos de variables condicionadas y no condicionadas.
    for factor in factors:
        condicionedSet = condicionedSet.union(factor.conditionedVariables())
        uncondicionedSet = uncondicionedSet.union(factor.unconditionedVariables())
    
    # Elimina cualquier variable que aparezca tanto en los conjuntos de variables no condicionadas como condicionadas.
    for uncondicioned in uncondicionedSet:
        if uncondicioned in condicionedSet:
            condicionedSet.remove(uncondicioned)
    
    # Obtener el dominio de las variables para el nuevo factor
    joined_domain = factors[0].variableDomainsDict()
    
    # Se crea un nuevo objeto Factor llamado joinedFactor utilizando los conjuntos de variables no condicionadas y condicionadas, y el dominio combinado de las variables del primer factor en la lista.
    joinedFactor = Factor(uncondicionedSet, condicionedSet, joined_domain)

    # Se obtienen todas las asignaciones posibles para el nuevo factor joinedFactor
    assignments = joinedFactor.getAllPossibleAssignmentDicts()
    
    # Este bucle itera sobre todas las asignaciones posibles para el nuevo factor joinedFactor.
    for assignment in assignments:
        # Establece la probabilidad de la asignación actual en joinedFactor a 1.
        joinedFactor.setProbability(assignment, 1)

        # Este bucle itera sobre todos los factores de la lista de entrada factors.
        for factor in factors:
            # Obtiene la probabilidad actual para la asignación actual en joinedFactor
            probFactor = joinedFactor.getProbability(assignment)
            # Obtiene la probabilidad actual para la asignación actual en el factor actual de la lista.
            probF = factor.getProbability(assignment)
            
            # Actualiza la probabilidad en joinedFactor multiplicándola por la probabilidad obtenida del factor actual.
            joinedFactor.setProbability(assignment, probFactor * probF)

    return joinedFactor
    "*** END YOUR CODE HERE ***"


def eliminateWithCallTracking(callTrackingList=None):

    def eliminate(factor, eliminationVariable):
        """
        Question 4: Your eliminate implementation 

        Input factor is a single factor.
        Input eliminationVariable is the variable to eliminate from factor.
        eliminationVariable must be an unconditioned variable in factor.
        
        You should calculate the set of unconditioned variables and conditioned 
        variables for the factor obtained by eliminating the variable
        eliminationVariable.

        Return a new factor where all of the rows mentioning
        eliminationVariable are summed with rows that match
        assignments on the other variables.

        Useful functions:
        Factor.getAllPossibleAssignmentDicts
        Factor.getProbability
        Factor.setProbability
        Factor.unconditionedVariables
        Factor.conditionedVariables
        Factor.variableDomainsDict
        """
        # autograder tracking -- don't remove
        if not (callTrackingList is None):
            callTrackingList.append(('eliminate', eliminationVariable))

        # typecheck portion
        if eliminationVariable not in factor.unconditionedVariables():
            print("Factor failed eliminate typecheck: ", factor)
            raise ValueError("Elimination variable is not an unconditioned variable " \
                            + "in this factor\n" + 
                            "eliminationVariable: " + str(eliminationVariable) + \
                            "\nunconditionedVariables:" + str(factor.unconditionedVariables()))
        
        if len(factor.unconditionedVariables()) == 1:
            print("Factor failed eliminate typecheck: ", factor)
            raise ValueError("Factor has only one unconditioned variable, so you " \
                    + "can't eliminate \nthat variable.\n" + \
                    "eliminationVariable:" + str(eliminationVariable) + "\n" +\
                    "unconditionedVariables: " + str(factor.unconditionedVariables()))

        "*** YOUR CODE HERE ***"

        #  Se obtienen las variables condicionadas y no condicionadas del factor actual
        unconditionedVar = factor.unconditionedVariables()
        conditionedVar = factor.conditionedVariables()

        # Se obtiene el diccionario de dominios de las variables del factor actual y se almacena en la variable f
        f = factor.variableDomainsDict()

        # Se elimina la variable especificada de las variables no condicionadas
        unconditionedVar.remove(eliminationVariable)
        # Se crea un nuevo factor sin la variable eliminada
        newFactor = Factor(unconditionedVar, conditionedVar, f)
        # Se obtienen todas las asignaciones posibles para las variables del nuevo factor.
        assignments = newFactor.getAllPossibleAssignmentDicts()
        
        for assignment in assignments:
            prob = 0.0

            # Se obtienen los posibles valores de la variable a eliminar
            eliminateVar = factor.variableDomainsDict()[eliminationVariable]

            for eliminate in eliminateVar:
                # Se actualiza la asignación para la variable eliminada.
                assignment[eliminationVariable] = eliminate
                # Se suma la probabilidad del factor original para la asignación actual.
                prob += factor.getProbability(assignment)
            
            # Se establece la probabilidad en el nuevo factor para la asignación actual.
            newFactor.setProbability(assignment, prob)
        return newFactor
        "*** END YOUR CODE HERE ***"

    return eliminate

eliminate = eliminateWithCallTracking()


def normalize(factor):
    """
    Question 5: Your normalize implementation 

    Input factor is a single factor.

    The set of conditioned variables for the normalized factor consists 
    of the input factor's conditioned variables as well as any of the 
    input factor's unconditioned variables with exactly one entry in their 
    domain.  Since there is only one entry in that variable's domain, we 
    can either assume it was assigned as evidence to have only one variable 
    in its domain, or it only had one entry in its domain to begin with.
    This blurs the distinction between evidence assignments and variables 
    with single value domains, but that is alright since we have to assign 
    variables that only have one value in their domain to that single value.

    Return a new factor where the sum of the all the probabilities in the table is 1.
    This should be a new factor, not a modification of this factor in place.

    If the sum of probabilities in the input factor is 0,
    you should return None.

    This is intended to be used at the end of a probabilistic inference query.
    Because of this, all variables that have more than one element in their 
    domain are assumed to be unconditioned.
    There are more general implementations of normalize, but we will only 
    implement this version.

    Useful functions:
    Factor.getAllPossibleAssignmentDicts
    Factor.getProbability
    Factor.setProbability
    Factor.unconditionedVariables
    Factor.conditionedVariables
    Factor.variableDomainsDict
    """

    # typecheck portion
    variableDomainsDict = factor.variableDomainsDict()
    for conditionedVariable in factor.conditionedVariables():
        if len(variableDomainsDict[conditionedVariable]) > 1:
            print("Factor failed normalize typecheck: ", factor)
            raise ValueError("The factor to be normalized must have only one " + \
                            "assignment of the \n" + "conditional variables, " + \
                            "so that total probability will sum to 1\n" + 
                            str(factor))

    "*** YOUR CODE HERE ***"
    
    totalProb = 0.0
    
    # Se obtienen todas las asignaciones posibles para las variables del factor.
    assignments = factor.getAllPossibleAssignmentDicts()

    for assignment in assignments:
        # Se obtiene la probabilidad asociada con la asignación actual
        probF = factor.getProbability(assignment)
        # La probabilidad obtenida se suma a la variable
        totalProb += probF

    unconditionedVar = factor.unconditionedVariables()
    conditionedVar = factor.conditionedVariables()

    unconditionedVar_aux = unconditionedVar.copy()
    conditionedVar_aux = conditionedVar.copy()

    for var in unconditionedVar:
        # Se obtiene el dominio de la variable actual
        values = factor.variableDomainsDict()[var]

        # Se calcula la cantidad de valores en el dominio de la variable.
        n_values = len(values)
        if n_values == 1:
            # Si la variable tiene un solo valor, significa que es condicionada y se elimina de la lista de variables no condicionadas
            unconditionedVar_aux.remove(var)
            # La variable se agrega a la lista de variables condicionadas
            conditionedVar_aux.add(var)
    # Se crea un nuevo factor utilizando las listas actualizadas de variables no condicionadas, condicionadas y el diccionario de dominios de variables original.
    new_factor = Factor(unconditionedVar_aux, conditionedVar_aux, variableDomainsDict)

    # Se obtienen nuevamente todas las asignaciones posibles para el factor original.
    assignments = factor.getAllPossibleAssignmentDicts()

    for assignment in assignments:
        # Se normaliza la probabilidad de cada asignación dividiendo la probabilidad original entre la suma total de las probabilidades
        probability_norm = factor.getProbability(assignment) / totalProb
        # Las probabilidades normalizadas se establecen en el nuevo factor.
        new_factor.setProbability(assignment,probability_norm)

    return new_factor

    "*** END YOUR CODE HERE ***"

